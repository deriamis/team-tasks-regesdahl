# External Product Evaluation

## Product: \<Product Name\>

### Executive Overview
Briefly describe the product, its features, and how those features do or do not meet GitLab's current needs. The description should be detailed enough that anyone who isn't familiar with the product can gain an understanding of how it operates, how it is developed and maintained, completeness and usability of documentation, etc. You will repeat some of this information in other sections - this description is for those who need to read information in complete sentences. Include in your description a brief analysis of whether the product could be distributed with GitLab and whether the developer/distributor has any competition with GitLab's products.

### Licenses
List the licenses offered by the distributor and under what conditions they apply (eg. dual-licensing, seat-based licensing, etc.) Specifically call out any licenses which are not compatible with the GitLab product.

### Costs
#### Direct product costs
#### Support costs
#### Hardware/hosting costs
#### Storage, transfer, and access costs
#### GitLab maintenance/engineering costs
#### Summary
Summarize the above cost categories and provide an estimate of the initial, quarterly, and/or yearly costs expected when using the product (including support contracts). Include the expected or estimated time that could be spent by GitLab engineers to migrate to the platform, diagnose problems, and interact with support (if applicable).

### Product Health and Maturity
Briefly describe each item below:

#### Overview
Product age, whether it stays up-to-date with current trends, feature completeness, developer/engineer activity, and whether it appears to have an abundance of unresolved bug reports and feature requests.
#### Community
Does the product has an active community apart from the core developers/engineers? Is the community supported or influenced by the product owner in any way?
#### Feature overview documentation
Do the features seem to be well-described and easy to understand?
#### Installation documentation
Are the installation docs complete and result in a "production" installation? Do they present alternative configurations? Do they present a clear, step-by-step method with either copy/paste commands or easy-to-understand examples?
#### Configuration documentation
Do the configuration docs fully describe the available options/arguments? Are the option/argument names clear and consistent? Are examples included for specific configuration patterns? Can the product be configured for high availability?
#### Troubleshooting guide/FAQ
Does the troubleshooting guide or FAQ actually seem to address likely problems? Do they rely on decoding cryptic messages or error codes? Are comprehensive logs available?

### Installation and Deployment
Select all that apply:

 - [ ] Managed/PaaS/SaaS
 - [ ] Self-hosted
 - [ ] Kubernetes
 - [ ] Helm Charts
 - [ ] Amazon Web Services
 - [ ] Google Cloud Platform
 - [ ] Docker
 - [ ] Other container
 - [ ] Dedicated hardware
 - [ ] Other (please describe):

### Release Cycle

 - [ ] Rolling/Bleeding-Edge
 - [ ] Weekly
 - [ ] Monthly
 - [ ] Quarterly
 - [ ] Semi-Annual
 - [ ] Yearly
 - [ ] Unscheduled/Unknown

### Support Availability
Select all that apply:
 - [ ] Enterprise/SLA Available
 - [ ] Paid (non-SLA)
 - [ ] SaaS/PaaS
 - [ ] Free
 - [ ] Email/Tickets
 - [ ] Slack
 - [ ] Discord
 - [ ] IRC
 - [ ] Security reporting
 - [ ] Abuse reporting
 - [ ] Other (please specify):

### Security Response
Select and briefly describe all that apply:

 - [ ] Bug bounty program:
 - [ ] CVE Numbering Authority:
 - [ ] Published patch release schedule:
 - [ ] Outstanding bug/security issues (of total):
 - [ ] Past-due security issues:
 - [ ] Dedicated security response team available:

### Source Availability

 - [ ] Proprietary
 - [ ] Closed-source (available for a fee)
 - [ ] Source available at no cost (may require an account)
 - [ ] Open Source (non-OSI)
 - [ ] Open Source (OSI)

### Governance and Stewardship
Select and briefly describe all that apply:

 - [ ] Open source funder:
 - [ ] Open source maintainer:
 - [ ] Employs open source developers:
 - [ ] Contributor policy/guidelines:
 - [ ] Community guidelines:

### Subjective Commentary
Any other items, concerns, or open questions not addressed by the above, such as:

 - Product does not meet our needs now or we are likely to outgrow it in the near future
 - Product focuses on commercial support to the exclusion of self-hosted users
 - Product distributor or support is a third-party with a tenuous connection to developers/engineers
 - Product does not seem of high quality or is "dying"
 - Documentation is not kept up-to-date
 - Support costs or GitLab engineer time that would be spent seems inordinately high
 - Product has a poor security record
 - Product leadership seems absent or arbitrary
 - Developers, engineers, or support staff have been observed to be rude or dismissive

/label ~"group::distribution" ~"devops::systems" ~"section::enablement" ~"type::feature"

/confidential 