Zendesk Ticket: < ZENDESK LINK>

Request:
 - From: < PERSON/TEAM>
 - Via: < MEDIUM>
 - For: < CUSTOMER >
 - Timezone: < CUSTOMER TIMEZONE >

> < REQUEST DETAILS >


---

_Please do not change anything below._

## Distribution Weekly DRI

- Apply label ~"Help group::Distribution Build" or ~"Help group::Distribution Deploy" per [each team's responsibility](https://handbook.gitlab.com/handbook/engineering/infrastructure/core-platform/systems/distribution/#target-consumers).
- Triage the issue and assess the priority.
- Resolve the issue per [weekly DRI priority](https://handbook.gitlab.com/handbook/engineering/infrastructure/core-platform/systems/distribution/workflow/#during-the-week).
- Route the issue accordingly if requires SME input or it is not with Distribution remit.

cc @plu8 @sissiyao @dorrino

/label ~"support request" ~"group::distribution" ~"devops::systems" 
/confidential
