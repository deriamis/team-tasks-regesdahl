## References

- [Kubernetes release post](<insert link here>)
- [Parent Epic: Support deploying GitLab on new versions of Kubernetes](https://gitlab.com/groups/gitlab-org/-/epics/11331)
- [GitLab Kubernetes release support policy](https://handbook.gitlab.com/engineering/infrastructure/core-platform/systems/distribution/k8s-release-support-policy.html)
- [Charts supported Kubernetes releases](https://docs.gitlab.com/charts/installation/cloud/#supported-kubernetes-versions)
- [Operator supported Kubernetes releases](https://docs.gitlab.com/operator/installation.html#cluster)


## Checklist

Follow the checklist below to address the requirements for each project.

### Administrative

- [ ] Change issue title to: `Support deploying GitLab to Kubernetes X.Y`
- [ ] Insert the Kubernetes release post URL above

### [GitLab Charts](https://gitlab.com/gitlab-org/charts/gitlab)

- [ ] Replace any removed APIs
- [ ] Create new Kubernetes cluster on GKE
  - [ ] Create new Agent configurations in [kube-agents project][kube-agents]
  - [ ] Save the Agent token in 1Password in the `Cloud Native` group in a new item named `cloud-native-vXYZ credentials`
  - [ ] Create a new cluster via the [kubernetes-provisioning project][kubernetes-provisioning]
  - [ ] Ensure the Agent is connected in [kube-agents/Kubernetes clusters][kube-agents/clusters]
- [ ] Create new Kubernetes cluster on EKS
  - [ ] Create new Agent configurations in [kube-agents project][kube-agents]
  - [ ] Save the Agent token in 1Password in the `Cloud Native` group in a new item named `eks charts-ci-cluster credentials`
  - [ ] Create a new cluster via the [kubernetes-provisioning project][kubernetes-provisioning]
  - [ ] Ensure the Agent is connected in [kube-agents/Kubernetes clusters][kube-agents/clusters]
- [ ] Add CI jobs to test against the new clusters
- [ ] Update CI `vcluster` [smoke test jobs](https://gitlab.com/gitlab-org/charts/gitlab/-/blob/master/.gitlab/ci/review-apps.gitlab-ci.yml)
      for the next Kubernetes release
- [ ] Update kubectl to `1.N.x` in the [CI base image](https://gitlab.com/gitlab-org/gitlab-build-images/-/blob/master/Dockerfile.gitlab-charts-build-base-helm-3.9?ref_type=heads)
- [ ] Remove old cluster CI jobs:
  - [ ] Remove CI `review_*` testing jobs for previous GKE and EKS clusters
  - [ ] Remove `Validate` jobs for Kubernetes versions that are no longer supported
  - [ ] Backport changes to the stable branches.
- [ ] Remove old clusters and agent configuration only after the changes above have been backported:
  - [ ] Remove the unsupported agent configuration from [kube-agents project][kube-agents].
  - [ ] Remove out-of-support GKE and EKS clusters
- [ ] Update supported releases documentation
  - [ ] Add entry for newly-supported Kubernetes version
  - [ ] Deprecate Kubernetes versions that have reached end-of-life
  - [ ] Remove entries that are no longer supported

### [GitLab Operator](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator)

- [ ] Replace any removed APIs
- [ ] Create new Kubernetes cluster on GKE
  - [ ] Create new Agent configurations in [kube-agents project][kube-agents]
  - [ ] Save the Agent token in 1Password in the `Cloud Native` group in a new item named `gitlab-operator-vXYZ credentials`
  - [ ] Create a new cluster via the [kubernetes-provisioning project][kubernetes-provisioning]
  - [ ] Ensure the Agent is connected in [kube-agents/Kubernetes clusters][kube-agents/clusters]
- [ ] Create new OpenShift cluster on GCP
  - [ ] Create new Agent configurations in [kube-agents project][kube-agents]
  - [ ] Save the Agent token in 1Password in the `Cloud Native` group in a new item named `ocp-ci-XYZ cluster credentials`
  - [ ] Create a new cluster via the [openshift-provisioning project][openshift-provisioning]
  - [ ] Ensure the Agent is connected in [kube-agents/Kubernetes clusters][kube-agents/clusters]
- [ ] Add CI jobs to test against the new cluster
- [ ] Update CI `vcluster` [smoke test jobs](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/tree/master/ci)
      for the next Kubernetes release
- [ ] Update kubectl to `1.N.x` in the [CI base image](https://gitlab.com/gitlab-org/gitlab-build-images/-/blob/master/Dockerfile.gitlab-operator-build-base-golang-1.21)
- [ ] Remove old cluster CI jobs and clusters
  - [ ] Remove CI `review_*` testing jobs for previous GKE and EKS clusters
  - [ ] Remove out-of-support GKE and EKS clusters
- [ ] Update supported releases documentation
  - [ ] Add entry for newly-supported Kubernetes version
  - [ ] Deprecate Kubernetes versions that have reached end-of-life
  - [ ] Remove entries that are no longer supported


/label ~"group::distribution" ~"devops::systems" ~"section::enablement"

/label ~"type::maintenance" ~"maintenance::pipelines" ~"group::distribution::build"

/label ~"For scheduling" ~"priority::2"

/epic https://gitlab.com/groups/gitlab-org/-/epics/11331

[kube-agents]: https://gitlab.com/gitlab-org/distribution/infrastructure/kube-agents
[kube-agents/clusters]: https://gitlab.com/gitlab-org/distribution/infrastructure/kube-agents/-/clusters
[kubernetes-provisioning]: https://gitlab.com/gitlab-org/distribution/infrastructure/kubernetes-provisioning
[openshift-provisioning]: https://gitlab.com/gitlab-org/distribution/infrastructure/openshift-provisioning
