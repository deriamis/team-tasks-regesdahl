## Process

- [Distribution DRI Role](https://handbook.gitlab.com/handbook/engineering/infrastructure/core-platform/systems/distribution/workflow/#distribution-dri)

## Duties (Copied from process page)

The Distribution DRI works on the following areas per the order of the list.

### During the week

1. Support incidents escalated from production.
1. [Support customer requests](https://handbook.gitlab.com/handbook/engineering/infrastructure/core-platform/systems/distribution/#engaging-distribution-for-expertise-in-support)
    - ([Distribution support request issues](https://gitlab.com/gitlab-org/distribution/team-tasks/-/issues/?sort=created_date&state=opened&label_name%5B%5D=support%20request&first_page_size=20)).
1. Answer or redirect questions in [#g_distribution](https://gitlab.slack.com/archives/C1FCTU4BE) Slack channel.
1. [Perform issue triage](https://handbook.gitlab.com/handbook/engineering/infrastructure/core-platform/systems/distribution/triage/).
1. Respond to `@gitlab-org/distribution` group mentions in GitLab.
1. **Optional:** Work on deliverables for the current milestone or other Distribution related tasks.

### Handover

1. Close the assigned DRI issue.
1. Rotate your name to the end of [the shift assignment list](https://docs.google.com/document/d/1ny8dB9N_jlt9cGCkNXlN5GpjTkOePUKvi-lnON2jBdA/edit#bookmark=id.nzrf04qngt3n).
1. Create a new issue for the next shift and assign it to the next team member on the list. The issue title should be `Distribution DRI rotation week of <starting date>`. Use the `Distribution-DRI` template to fill in the description.
1. For requests still active at the time of handoff, the DRI should use best judgement and consider one or several options:
    - Close the request with a documented mitigation and open a follow-up issue.
    - Add a comment that hands off the request to the next week's DRI.
    - Continue to act as SME, though handoff to the next DRI with clear documentation to share knowledge is strongly preferred.
    - Pass information to the next DRI in a synchronous conversation.

## Triage duty issue searches

### Issues to triage

- [Omnibus issues to triage](https://gitlab.com/gitlab-org/omnibus-gitlab/-/issues/?assignee_id=None&milestone_title=None&not%5Blabel_name%5D%5B%5D=For%20Scheduling&not%5Blabel_name%5D%5B%5D=awaiting%20feedback&not%5Blabel_name%5D%5B%5D=needs%20investigation&not%5Blabel_name%5D%5B%5D=pipeline%20failure&scope=all&sort=created_date&state=opened&not%5Btype%5D%5B%5D=task)
- [Charts issues to triage](https://gitlab.com/gitlab-org/charts/gitlab/-/issues/?assignee_id=None&milestone_title=None&not%5Blabel_name%5D%5B%5D=For%20Scheduling&not%5Blabel_name%5D%5B%5D=awaiting%20feedback&not%5Blabel_name%5D%5B%5D=needs%20investigation&not%5Blabel_name%5D%5B%5D=FedRamp%3A%3AWaiting%20on%20Vendor&not%5Blabel_name%5D%5B%5D=pipeline%20failure&scope=all&sort=created_date&state=opened&not%5Btype%5D%5B%5D=task)
- [Operator issues to triage](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/issues?assignee_id=None&milestone_title=None&not%5Blabel_name%5D%5B%5D=For+Scheduling&not%5Blabel_name%5D%5B%5D=awaiting+feedback&not%5Blabel_name%5D%5B%5D=needs+investigation&scope=all&sort=created_date&state=opened&not[type][]=task&not[label_name][]=pipeline+failure)
- [GitLab issues to triage](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=opened&assignee_id=None&milestone_title=None&not%5Blabel_name%5D%5B%5D=For%20Scheduling&not%5Blabel_name%5D%5B%5D=awaiting%20feedback&not%5Blabel_name%5D%5B%5D=needs%20investigation&not%5Blabel_name%5D%5B%5D=pipeline%20failure&not%5Btype%5D%5B%5D=task&label_name%5B%5D=group%3A%3Adistribution&first_page_size=100)

### Issues awaiting feedback

- Omnibus issue list [`awaiting feedback`, sorted by `Updated date`](https://gitlab.com/gitlab-org/omnibus-gitlab/-/issues?assignee_id=None&label_name%5B%5D=awaiting+feedback&milestone_title=None&page=3&scope=all&sort=updated_desc&state=opened&not[type][]=task) (start from oldest updated issue)
- Chart issue list [`awaiting feedback`, sorted by `Updated date`](https://gitlab.com/gitlab-org/charts/gitlab/-/issues?label_name%5B%5D=awaiting+feedback&milestone_title=None&scope=all&state=opened&not[type][]=task) (start from oldest updated issue)
- Operator issue list [`awaiting feedback`, sorted by `Updated date`](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/issues?label_name%5B%5D=awaiting+feedback&milestone_title=None&scope=all&state=opened&not[type][]=task) (start from oldest updated issue)
- GitLab issue list [`awaiting feedback`, sorted by `Updated date`](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=updated_asc&state=opened&label_name%5B%5D=awaiting%20feedback&label_name%5B%5D=group%3A%3Adistribution&milestone_title=None&not%5Btype%5D%5B%5D=task&first_page_size=100) (start from oldest updated issue)

### Issues for pipeline failures

- [Issues board (aggregated)](https://gitlab.com/groups/gitlab-org/-/boards/4076077?label_name%5B%5D=pipeline%20failure&label_name%5B%5D=group%3A%3Adistribution).
- [Omnibus pipeline failure issue board](https://gitlab.com/gitlab-org/omnibus-gitlab/-/boards/3211630?label_name[]=pipeline%20failure)
- [CNG pipeline failure issue board](https://gitlab.com/gitlab-org/charts/gitlab/-/boards/3503121?label_name[]=pipeline%20failure)
- [Operator pipeline failure issue board](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/boards/3091865?label_name[]=pipeline%20failure)

## Reference

- [Frequently used labels](https://gitlab.com/gitlab-org/distribution/team-tasks/-/blob/master/frequently-used-labels.md)
- ~Triage [issue history](https://gitlab.com/gitlab-org/distribution/team-tasks/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name%5B%5D=Triage)

/label ~Triage ~"group::distribution" ~"devops::systems" ~"section::enablement"
/due next friday
