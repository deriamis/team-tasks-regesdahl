<!--
# READ ME FIRST

This issue is a resource in addition to the general on-boarding task.
It is meant to create an easier overview of what you need to do to get
up to speed to team related tasks faster.

You are still required to finish the general on-boarding task even if
you close this issue. Once both of the issues are closed, you can
consider your onboarding finished.

-->

# New team member

Welcome to the Distribution team!

To get you up to speed quicker so you can start contributing to the team efforts, this on-boarding issue will try to provide a few tips on how to navigate around the team resources as well as company resources.

First, you are likely to want to know where your place is in the company structure. For this you would want to check out [team chart](https://about.gitlab.com/team/chart/).

## Company

Most of the information you will ever need is listed in your general onboarding
document. However, there is a lot of things to digest in there so let's highlight few items that you should focus on.

Understanding the [Values](https://handbook.gitlab.com/handbook/values/) is really important as that sets the tone for all interactions that you will have in the company. If anything is unclear about it, feel free to ask a question in your next 1-1 with your Engineering Manager.
Sooner or later you will have some money related question, so be sure to checkout
[spending company money page](https://about.gitlab.com/handbook/spending-company-money/).

Reading through the entire company handbook has become an impossible task, so please focus on the areas called out in your general onboarding issue, and when you have an additional question, make use of the handbook search. In addition, the Distribution team has a [further reading](https://about.gitlab.com/handbook/engineering/development/enablement/systems/distribution/#further-reading) list that includes links to areas of the handbook that the team has found the most important for guiding how we work. Please check it out.

For now though, you should get to know your team a bit better.

## Team

Each team has its own handbook section, and Distribution is no exception.
Read through the whole [Distribution handbook](https://about.gitlab.com/handbook/engineering/development/enablement/systems/distribution/)
section please!
Consider this your first team task, understanding the Mission, Vision and how work is executed within the team. If you find any typo's or items that could be made clearer, please consider [editing that page](https://gitlab.com/-/ide/project/gitlab-com/content-sites/handbook/edit/main/-/content/handbook/engineering/infrastructure/core-platform/systems/distribution/_index.md) by submitting a Merge Request. This is how you can start a discussion with your colleagues. If you would rather start a discussion first, you can go to the
[Distribution team tasks issue tracker](https://gitlab.com/gitlab-org/distribution/team-tasks) and submit an issue there.

## Team Onboarding Tasks

### New team member

- [ ] Review [Distribution handbook section](https://about.gitlab.com/handbook/engineering/development/enablement/systems/distribution/).
- [ ] Check out [team projects](https://about.gitlab.com/handbook/engineering/development/enablement/systems/distribution/#all-projects).
- [ ] After joining Slack, join the following channels:
    - #g_distribution: This is where team discussions and other groups make contact with the Distribution team.
    - #distribution-lounge: This is casual lounge for Distribution team.
    - #engineering-fyi: This is mandatory for the engineering organization.
    - #kubernetes: This is a useful channel for any Distribution team member.
    - #loc_*: There are location based channels for employees all over the world. Look through the channels that start with `loc_` to find the one for your area.
- [ ] Verify that you have access to [all work resources](https://about.gitlab.com/handbook/engineering/development/enablement/systems/distribution/#work-resources) listed.
- [ ] Read the [Keeping yourself informed](https://handbook.gitlab.com/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/getting-started/keeping-yourself-informed/) documentation.
- [ ] Complete the [Secure Coding training](https://handbook.gitlab.com/handbook/security/secure-coding-training/).
- [ ] Create an account at [ArtifactHub.io](https://artifacthub.io) using your GitLab email account. Once the account has been confirmed (link sent to email), inform your manager so that the account can be associated with the GitLab organization.
- [ ] Activate GitLab Sandbox account by logging into https://gitlabsandbox.cloud via Okta, and then notify the manager, so the manager can add you to the team shared AWS account. Also familiarize with [Gitlab Sandbox handbook page](https://handbook.gitlab.com/handbook/infrastructure-standards/realms/sandbox/).
- [ ] Review and familiarize yourself with the Engineering [workflow](https://about.gitlab.com/handbook/engineering/workflow/) and [Distribution Team Workflow](https://about.gitlab.com/handbook/engineering/development/enablement/systems/distribution/workflow.html)
- [ ] [Request a ZenDesk 'Light Agent' account](https://about.gitlab.com/handbook/support/internal-support/#requesting-a-zendesk-light-agent-account) to be able to view customer support tickets in Zendesk
- [ ] Add the Distribution time off calendar to your calendars and to PTO by Deel.
    - calendarid: `gitlab.com_eo66o3d55rdh2v36o5rlthh528@group.calendar.google.com`
    - [Instructions](https://about.gitlab.com/handbook/people-group/engineering/team-pto-calendar/)
- [ ] Visit (and bookmark) [merge requests ready for review](https://gitlab-org.gitlab.io/distribution/monitoring/mrs/) page
- [ ] (Optional) [Request GitLab Ultimate Tier](https://about.gitlab.com/handbook/incentives/#gitlab-ultimate) for account
- [ ] (Optional) Check [Distribution Team Training](https://about.gitlab.com/handbook/engineering/development/enablement/systems/distribution/training.html) listings

### Manager

- [ ] Add new team member to [gitlab-org/distribution](https://gitlab.com/gitlab-org/distribution)
- [ ] Add new team member to [Distribution Google Group](https://groups.google.com/a/gitlab.com/g/distribution/members)
- [ ] Submit access request for adding new member to ([Issue template](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request), [Example](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/17267)):
  - **For backend engineers:**
    - Google group `gcp-distribution-sg@gitlab.com`, in order to access the following GCP projects:
      - cloud-native
      - omnibus-runners
      - testground (likely from baseline entitlement, TBC)
    - 1Password vaults
        - `build`
        - `cloud native`
        - ~~`cloud images` (likely outdated,TBC)~~
    - Slack group `distribution`
  - **For test engineers:**
    - Request access to the `cloud-native` GCP project as roles `Kubernetes Engine Admin` and `Editor`
    - 1Password vault(s)
        - `cloud native`
    - Slack group `distribution`
- [ ] Submit access request for the following AWS account:
  - dev-distribution ([Issue template](https://gitlab.com/gitlab-com/business-technology/engineering/infrastructure/issue-tracker/-/issues/new?issuable_template=aws_group_account_iam_update_request), [Example](https://gitlab.com/gitlab-com/business-technology/engineering/infrastructure/issue-tracker/-/issues/376))
- [ ] [Request Red Hat account for new team member](https://www.redhat.com/wapps/ugc/protected/usermgt/userList.html)

For access requests, you can submit the access requests by creating a [new issue](https://gitlab.com/gitlab-com/access-requests/issues/new) in gitlab-com/access-requests. Use the New Access Request template for the issue, but this template can be a bit daunting and you can create a comment on this issue to receive some assistance.

## Staying informed

There is a lot of information flowing around, but to stay on top of most important things you should be a part of `#whats-happening-at-gitlab` channel in Slack.

As for the team events, you are not obligated to be part of any of the meetings
that you are invited to. However, please note your absence by responding to invites in time. Not attending the meeting is no excuse for not reading the
agenda items or going through the notes of the meeting.

## On-boarding experience summary

To share some new found knowledge, the rest of the Distribution team would love
to hear your experience.

This is why in first Distribution team meeting 7 days after your start, we would
like to hear from you about:

* Most interesting information you found and did not know about the company
* Most interesting information you found and did not know about the team
* Piece of information you could not find about the company and the team
* Information that you think could use some more details/improvement

Add yourself to the list of speakers in the Distribution Team weekly sync with bullet points from above, and
spend 3-5 minutes max in total sharing your experience with the team.

/label ~"onboarding"
