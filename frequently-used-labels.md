# Frequently Used Labels

These are labels frequently used by the Distribution team on labels and issues. These labels can also be used to
determine with of the Distribution teams is responsible for a issue, if the issue is within a [Distribution owned project](https://about.gitlab.com/handbook/engineering/development/enablement/systems/distribution/#all-projects),
and has one of the team labels below. These labels are also shared throughout the organization, but may be used with
different teams or meanings in projects that are not owned by the Distribution Team.

## Shared labels

These should be used in addition to other labels that better determine which team owns the issue.

| Name | labels | Description |
| ---- | ------ | ---- |
| PostgreSQL | ~database ~postgresql | Changes related PostgreSQL |
| NGINX | ~nginx |  Changes related to NGINX |
| Consul | ~consul |  Changes related to Consul |
| PGBouncer | ~pgbouncer | Changes related to PGBouncer |
| Redis | ~redis | Changes related to Redis |
| Distribution Process Improvement | ~"process-improvement" | Team/Project process improvements |
| Cross-team work | ~"cross-group" | Work that is owned by a different group, or multiple groups, but being working on by us |
| Distribution Housecleaning | ~cleanup | Issue/MR/Code/infra/service cleanup tasks  |
| Omnibus/Cloud Native Service | ~service-component |  Relating to Services that make up GitLab |
| Distribution Docker | ~Docker | Related to docker images/containers |
| Upstream Contribution | ~"Upstream contribution" | Tracks changes submitted to upstream open source projects |
| Upstream Dependency Blocker | ~"upstream deps" | Is waiting or blocked due to upstream bug/change |
| Tests | ~test | Test related changes |

## Build Labels (~"group::distribution::build")

These labels are frequently used on ~"group::distribution::build" issues.

| Name | labels | Description |
| ---- | ------ | ----------- |
| GitLab Build Tooling | ~build-toolchain | Related to the software we use to build GitLab |
| GitLab Build Infrastructure | ~build-infra | Related to the infrastructure we use to build GitLab |
| Distribution Pipelines | ~"maintenance::pipelines" | Related to CI pipelines |
| Distribution Pipeline feature updates | ~"maintenance::pipelines" ~Dogfooding | Updating pipelines to make use of new gitlab CI features |
| GitLab Packages | ~os-packages | Changes related to the .deb/.rpm packages |
| Distribution Dependency updates | ~"maintenance::dependency" | Dependency updates |
| Cloud Native Images | ~cloud-native-images | CNG images |
| GitLab Package Images | ~package-images | Omnibus Docker images |
| Selinux | ~SELinux | Changes required for SELinux |
| Federal Information Processing Standard (FIPS) | ~FIPS | FIPS related work |
| Marketplaces |  ~marketplace | Related to maintaining GitLabs marketplace presence |
| Bundled Service/Platform Deprecations | ~deprecation ~"group::distribution::build" | Deprecations for supported platforms, versions of Linux distributions, or bundled services. Examples include CentOS 6, unicorn, repmgr. |
| Distribution License Validation | ~software-licensing | Related to software licenses we bundle |
| ARM | ~"cpu::arm64" | Related to GitLab on ARM |
| s390x | ~"cpu::s390x" | Related to GitLab on s390x |
| Platform Specific | ~platform-specific | Outlier that relates to a single distribution, platform, or architecture (eg Debian/AWS/ARM/) |
| Source Compiles | ~source-compile | Compiling software from source code |
| Build Pipelines | ~"maintenance::pipelines" ~source-compile | Pipeline changes for building software |
| Test Pipelines | ~"maintenance::pipelines" ~test | Pipeline changes specific to tests or verification |

## Deploy Labels (~"group::distribution::deploy")

These labels are frequently used on ~"group::distribution::deploy" issues.

| Name | labels | Description |
| ---- | ------ | ----------- |
| GitLab Installation | ~installation | Related to installing GitLab software |
| GitLab Updates | ~upgrades | Related to upgrading GitLab software |
| GitLab Configuration | ~"configuration files" | Changes to software configuration files |
| GitLab Configuration Defaults | ~default-config | Changes to default configuration values |
| Config/Feature Deprecations | ~deprecation ~"Distribution:Deploy" | Deprecations for configuration or deployment features |
| Kubernetes (k8s) Platform | ~k8s-platform | Related to a single platform like okd, rke, eks, gke, ack, aks, k3s, kind, mirok8s, minikube |
| Deployment Automation | ~deploy-automation | Chef recipes/Terraform/Ansible/Rancher/Helm |
| Single node | ~single-node | Related to Deploying single node installations |
| High Availability (HA) | ~HA | Related to Deploying to multi-node installations |
| Helm | ~helm | Helm and chart requirements compatibility |
| GitLab Cloud Native Chart | ~"Category:Cloud Native Installation" ~gitlab-chart | Most Kubernetes object changes |
| GitLab Cloud Native Operator | ~"Category:Cloud Native Installation" ~gitlab-operator | Most operator functionality |
